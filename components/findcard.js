class MyCircle extends HTMLElement {
  constructor() {
      super();
      this.shadow = this.attachShadow({mode: 'open'});
  }
  connectedCallback() {
      var countProg = this.getAttribute("count");
      var nameProg = this.getAttribute("name");
      var descProg = this.getAttribute("desc");
      var imgProg = this.getAttribute("img");
      this.render(countProg,nameProg,descProg,imgProg);
  }

  render(countProg,nameProg,descProg,imgProg) {
      this.shadow.innerHTML = `
          <style>
              img {
                  width: 100%;
                  height: 100%;
                  z-index : 1;
              }
              .card-inner-container {
                display : flex;
              }
              .img-count-container {
                min-width : 200px;
                max-width : 200px;
                max-height : 200px;
                position : relative;
                margin : 20px;
              }
              .img-count-counter {
                position : absolute;
                right : 0;
                margin : 0;
              }
              .info-container {
                margin : 20px;
              }

          </style>
          <div class="card-inner-container">
              <div class="img-count-container">
                <h2 class="img-count-counter">${countProg}</h2>
                <img src="${imgProg}">
              </div>
              <div class="info-container">
                <h2>${nameProg}</h2>
                <p>${descProg}</p>
              </div>

          </div> 
      `;
  }


}
customElements.define('my-circle', MyCircle);